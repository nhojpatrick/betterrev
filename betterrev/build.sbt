import play.Project._

javacOptions += "-Xlint:unchecked"

scalacOptions += "-feature"

scalacOptions += "-language:reflectiveCalls"

name := "betterev"

version := "1.0.0-SNAPSHOT"

playJavaSettings

libraryDependencies ++= Seq(
  javaEbean,
  "org.avaje.ebeanorm" % "avaje-ebeanorm-api" % "3.1.1", 
  "play2-crud" % "play2-crud_2.10" % "0.7.3-SNAPSHOT" exclude("org.scala-stm", "scala-stm_2.10.0"),
  "com.typesafe.akka" % "akka-testkit_2.10" % "2.2.2" exclude("org.scala-stm", "scala-stm_2.10.0"),
  "com.aragost.javahg" % "javahg" % "0.4" exclude("org.scala-stm", "scala-stm_2.10.0"),
  "postgresql" % "postgresql" % "9.1-901.jdbc4" exclude("org.scala-stm", "scala-stm_2.10.0"),
  "com.goldmansachs" % "gs-collections" % "4.2.0" exclude("org.scala-stm", "scala-stm_2.10.0"),
  "com.icegreen" % "greenmail" % "1.3.1b" exclude("org.scala-stm", "scala-stm_2.10.0"),
  "net.sourceforge.htmlcleaner" % "htmlcleaner" % "2.6" exclude("org.scala-stm", "scala-stm_2.10.0")
)

resolvers += "release repository" at "http://hakandilek.github.com/maven-repo/releases/"

resolvers += "snapshot repository" at "http://hakandilek.github.com/maven-repo/snapshots/"

