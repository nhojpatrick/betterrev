package utils;

import play.libs.WS;

public final class FetchDiffFilesString {
    
    private final static long TWO_SECOND_TIMEOUT = 2000;
    
    private FetchDiffFilesString() {
        //  Hide Utility Class Constructor - Utility classes should not have a public or default constructor.
    }

    public static String from(String inPullRequestURL) {
        WS.Response response = WS.url(inPullRequestURL).get().get(TWO_SECOND_TIMEOUT);
        return response.getBody();
    }
}
