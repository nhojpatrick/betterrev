package update.pullrequest;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author Edward Yue Shung Wong
 */
public final class ImportPullRequestsEvent {
    private final JsonNode jsonNode;
    private final String project;

    public ImportPullRequestsEvent(JsonNode jsonNode, String project) {
        this.jsonNode = jsonNode;
        this.project = project;
    }

    public JsonNode getJsonNode() {
        return jsonNode;
    }

    public String getProject() {
        return project;
    }
}
